var ReplugApp = angular.module('ReplugApp', []);

ReplugApp.controller('LoginCtrl', function($scope, $timeout){
	$scope.userInfo = {};
	$scope.showForm = true;
	$scope.showLoader = false;
	$scope.showResult = false;

	$scope.onSubmit = function(userInfo){
		$scope.showForm = false;
		$scope.showLoader = true;

		$timeout(function() {

			$scope.showLoader = false;
			$scope.showResult = true;
			$scope.loggedUser = userInfo.username;
			$scope.loggedPassword = userInfo.password;

		}, 2000);

	};

	$scope.doLogout = function(){
		$scope.userInfo = {};
		$scope.showForm = true;
		$scope.showLoader = false;
		$scope.showResult = false;

	};
})
